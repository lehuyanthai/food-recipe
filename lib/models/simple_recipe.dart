class SimpleRecipe {
  String dishImage;
  String title;
  String duration;
  String source;
  String recipe;
  List<String> information;

  SimpleRecipe(this.dishImage, this.title, this.duration, this.source,
      this.recipe, this.information);

  SimpleRecipe.fromJson(Map<String, dynamic> json) {
    dishImage = json['dishImage'];
    title = json['title'];
    duration = json['duration'];
    source = json['source'];
    recipe = json['recipe'];
    information = json['information'].cast<String>();
  }
}
