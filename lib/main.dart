import 'package:flutter/material.dart';
import 'package:project1/foodrecipe_theme.dart';
import 'package:project1/home.dart';

void main() {
  runApp(const Fooderlich());
}

class Fooderlich extends StatelessWidget {
  const Fooderlich({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: FooderlichTheme.dark(),
      title: 'Food Recipe',
      home: const Home(),
    );
  }
}
