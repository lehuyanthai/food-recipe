import 'package:flutter/material.dart';
import 'package:project1/models/simple_recipe.dart';
import 'package:project1/screens/details_recipe.dart';

class RecipeThumbnail extends StatefulWidget {
  final SimpleRecipe recipe;

  const RecipeThumbnail({Key key, this.recipe}) : super(key: key);

  @override
  _RecipeThumbnailState createState() => _RecipeThumbnailState();
}

class _RecipeThumbnailState extends State<RecipeThumbnail> {
  bool liked = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: InkWell(
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DetailsRecipe(
                          recipe: widget.recipe,
                        )),
              )
            },
            child: ClipRRect(
                child: Image.asset('${widget.recipe.dishImage}',
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(12)),
          )),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.recipe.title,
                      maxLines: 1,
                      style: Theme.of(context).textTheme.bodyText1),
                  Text(widget.recipe.duration,
                      style: Theme.of(context).textTheme.bodyText1),
                ],
              ),
              IconButton(
                  icon: Icon(
                    liked ? Icons.favorite : Icons.favorite_border_outlined,
                    color: Colors.white,
                  ),
                  onPressed: () {
                    setState(() {
                      liked = !liked;
                    });
                  })
            ],
          ),
        ],
      ),
    );
  }
}
