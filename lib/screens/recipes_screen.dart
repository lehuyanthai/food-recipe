import 'package:flutter/material.dart';
import 'package:project1/api/mock_fooderlich_service.dart';
import 'package:project1/components/recipes_grid_view.dart';

class RecipesScreen extends StatelessWidget {
  final exploreService = MockFooderlichService();

  RecipesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
          future: exploreService.getRecipes(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return RecipesGridView(recipes: snapshot.data);
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
}
