import 'package:flutter/material.dart';
import 'package:project1/components/circle_image.dart';
import 'package:project1/models/post.dart';

class FriendPostTile extends StatelessWidget {
  final Post post;

  const FriendPostTile({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleImage(
              imageProvider: AssetImage('${post.profileImageUrl}'),
              imageRadius: 20),
          const SizedBox(width: 16),
          Expanded(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                Text(post.comment),
                Text('${post.timestamp} mins ago',
                    style: Theme.of(context).textTheme.bodyText1)
              ]))
        ]);
  }
}
