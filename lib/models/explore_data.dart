import 'package:project1/models/explore_recipe.dart';
import 'package:project1/models/post.dart';

class ExploreData {
  final List<ExploreRecipe> todayRecipes;
  final List<Post> friendPosts;

  ExploreData(this.todayRecipes, this.friendPosts);
}
