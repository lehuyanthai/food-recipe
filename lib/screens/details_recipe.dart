import 'package:flutter/material.dart';
import 'package:project1/foodrecipe_theme.dart';
import 'package:project1/models/simple_recipe.dart';

class DetailsRecipe extends StatelessWidget {
  final SimpleRecipe recipe;
  const DetailsRecipe({Key key, this.recipe}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Container(
            child: Stack(
              alignment: AlignmentDirectional.bottomCenter,
              overflow: Overflow.visible,
              children: [
                Positioned(
                  child: IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: 20,
                      ),
                      onPressed: () => Navigator.pop(context)),
                  top: 16,
                  left: 16,
                ),
                Positioned(
                  child: Container(
                    width: 300,
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.grey.shade900,
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(top: 8.0, left: 16, right: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            recipe.title,
                            style: FooderlichTheme.darkTextTheme.headline2,
                          ),
                          Text(
                            recipe.information[2],
                            style: FooderlichTheme.darkTextTheme.headline3,
                          ),
                          SizedBox(
                            height: 10.0,
                            child: new Center(
                              child: new Container(
                                height: 1.0,
                                color: Colors.grey.shade300,
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Icon(
                                    Icons.access_time,
                                    color: Colors.white,
                                  ),
                                  Text(recipe.duration)
                                ],
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.baby_changing_station,
                                    color: Colors.white,
                                  ),
                                  Text(recipe.information[3])
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  top: 238,
                )
              ],
            ),
            // padding: const EdgeInsets.all(16),
            constraints: const BoxConstraints.expand(width: 350, height: 300),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(recipe.dishImage),
                fit: BoxFit.cover,
              ),
              borderRadius: const BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10)),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                height: 30,
              ),
              Text(
                'Recipe',
                style: FooderlichTheme.darkTextTheme.headline1,
              ),
              Text(recipe.recipe)
            ]),
          )
        ],
      ),
    );
  }
}
